<?php

function commerce_sdek_variable_group_info() {
  $groups = array();

  $groups['commerce_sdek_connection_settings'] = array(
    'title' => t('Connection settings'),
    'access' => 'sdek connection settings',
  );
  $groups['commerce_sdek_delivery_settings'] = array(
    'title' => t('Delivery settings'),
    'access' => 'sdek delivery settings',
  );
  $groups['commerce_sdek_order_tokens'] = array(
    'title' => t('Order info tokens settings'),
    'access' => 'sdek order tokens settings',
  );

  return $groups;
}

function commerce_sdek_variable_info() {
  $variables = array();

  $variables['commerce_sdek_server_api_url'] = array(
    'title' => t('API server URL'),
    'group' => 'commerce_sdek_connection_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 24,
    ),
    'default' => 'https://integration.cdek.ru/',
    'token' => TRUE,
    'access' => 'sdek connection settings',
  );
  $variables['commerce_sdek_server_timeout'] = array(
    'title' => t('API server timeout'),
    'group' => 'commerce_sdek_connection_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 3,
      '#field_suffix' => 'sec.',
    ),
    'default' => 60,
    'token' => TRUE,
    'access' => 'sdek connection settings',
  );
  $variables['commerce_sdek_server_login'] = array(
    'title' => t('Login'),
    'group' => 'commerce_sdek_connection_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 32,
    ),
    'token' => TRUE,
    'access' => 'sdek connection settings',
  );
  $variables['commerce_sdek_server_password'] = array(
    'title' => t('Password'),
    'group' => 'commerce_sdek_connection_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 32,
    ),
    'token' => TRUE,
    'access' => 'sdek connection settings',
  );

  $variables['commerce_sdek_send_city_code'] = array(
    'title' => t('Warehouse City Code'),
    'group' => 'commerce_sdek_delivery_settings',
    'element' => array(
      '#type' => 'select',
    ),
    'options callback' => 'commerce_sdek_get_cities_list',
    'default' => '',
    'access' => 'sdek delivery settings',
  );
  $sdek_tariffs = array(
    136 => 'Посылка склад-склад',
    137 => 'Посылка склад-дверь',
    138 => 'Посылка дверь-склад',
    139 => 'Посылка дверь-дверь',
    233 => 'Экономичная посылка склад-дверь',
    234 => 'Экономичная посылка склад-склад',
    301 => 'До постомата InPost дверь-склад',
    302 => 'До постомата InPost склад-склад',
    291 => 'CDEK Express склад-склад',
    293 => 'CDEK Express дверь-дверь',
    294 => 'CDEK Express склад-дверь',
    295 => 'CDEK Express дверь-склад',
  );
  $variables['commerce_sdek_courier_tariff_code'] = array(
    'title' => t('Courier Tariff'),
    'group' => 'commerce_sdek_delivery_settings',
    'element' => array(
      '#type' => 'select',
      '#options' => $sdek_tariffs,
    ),
    'default' => 137,
    'access' => 'sdek delivery settings',
  );
  $variables['commerce_sdek_self_service_tariff_code'] = array(
    'title' => t('Self-Service Tariff'),
    'group' => 'commerce_sdek_delivery_settings',
    'element' => array(
      '#type' => 'select',
      '#options' => $sdek_tariffs,
    ),
    'default' => 136,
    'access' => 'sdek delivery settings',
  );
  $variables['commerce_sdek_addon_fitting'] = array(
    'title' => t('Add fitting service to all orders'),
    'group' => 'commerce_sdek_delivery_settings',
    'element' => array(
      '#type' => 'checkbox',
    ),
    'default' => TRUE,
    'access' => 'sdek delivery settings',
  );
  $variables['commerce_sdek_addon_partial_delivery'] = array(
    'title' => t('Allow customer to partially accept order content'),
    'group' => 'commerce_sdek_delivery_settings',
    'element' => array(
      '#type' => 'checkbox',
    ),
    'default' => TRUE,
    'access' => 'sdek delivery settings',
  );
  $variables['commerce_sdek_addon_customer_open_box'] = array(
    'title' => t('Allow customer to open box'),
    'group' => 'commerce_sdek_delivery_settings',
    'element' => array(
      '#type' => 'checkbox',
    ),
    'default' => TRUE,
    'access' => 'sdek delivery settings',
  );
  $variables['commerce_sdek_addon_weekend_delivery'] = array(
    'title' => t('Allow SDEK to delivery order during weekend'),
    'group' => 'commerce_sdek_delivery_settings',
    'element' => array(
      '#type' => 'checkbox',
    ),
    'default' => FALSE,
    'access' => 'sdek delivery settings',
  );

  $variables['commerce_sdek_tokens_Number'] = array(
    'title' => t('Order Id'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'default' => '[commerce-order:order-id]',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_tokens_RecipientName'] = array(
    'title' => t('Customer Name'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'default' => '[commerce-order:commerce-customer-shipping:commerce-customer-address:name-line]',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_tokens_Phone'] = array(
    'title' => t('Customer Phone'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'default' => '[commerce-order:commerce-customer-shipping:field-customer-phone]',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_tokens_RecipientEmail'] = array(
    'title' => t('Customer Email'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'default' => '[commerce-order:mail]',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_tokens_Comment'] = array(
    'title' => t('Order Comment'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'default' => '[commerce-order:field_order_comment]',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_tokens_AddressStreet'] = array(
    'title' => t('Customer Address Street'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'default' => '[commerce-order:commerce-customer-shipping:commerce-customer-address:thoroughfare]',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_tokens_AddressHouse'] = array(
    'title' => t('Customer Address House'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'default' => '[commerce-order:commerce-customer-shipping:commerce-customer-address:thoroughfare]',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_tokens_AddressFlat'] = array(
    'title' => t('Customer Address Flat'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'default' => '[commerce-order:commerce-customer-shipping:commerce-customer-address:thoroughfare]',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_tokens_AddressComment'] = array(
    'title' => t('Customer Address Comment'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'default' => '[commerce-order:commerce-customer-shipping:commerce-customer-address:thoroughfare]',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_tokens_AddressCity'] = array(
    'title' => t('Customer Address City'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'default' => '[commerce-order:commerce-customer-shipping:commerce-customer-address:locality]',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_tokens_Amount'] = array(
    'title' => t('Amount to be collected'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'default' => '[commerce-order:order-payment-balance-amount-decimal]',
    'access' => 'sdek order tokens settings',
  );
  //$variables['commerce_sdek_tokens_ValuatedAmount'] = array(
  //  'title' => t('Order Valuated Amount (order total)'),
  //  'group' => 'commerce_sdek_order_tokens',
  //  'element' => array(
  //    '#type' => 'textfield',
  //    '#size' => 120,
  //  ),
  //  'default' => '[commerce-order:commerce-order-total:amount-decimal]',
  //  'access' => 'sdek order tokens settings',
  //);
  $variables['commerce_sdek_tokens_ProductWeight'] = array(
    'title' => t('Product Weight'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'default' => '1',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_tokens_Volume'] = array(
    'title' => t('Number Of Allocated spaces'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'default' => '1',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_use_delivery_dates'] = array(
    'title' => t('Use delivery date'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'checkbox',
    ),
    'default' => FALSE,
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_tokens_DeliveryDate'] = array(
    'title' => t('Delivery date'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'default' => '[commerce-order:field-order-delivery-date:value:custom:d-m-Y]',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_tokens_TimeBeg'] = array(
    'title' => t('Delivery Time period beginning'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'default' => '[commerce-order:field-order-delivery-date:value:custom:H-i]',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_tokens_TimeEnd'] = array(
    'title' => t('Delivery Time period end'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'default' => '[commerce-order:field-order-delivery-date:value2:custom:H-i]',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_tokens_ServicePointExternalCode'] = array(
    'title' => t('Service point ExternalCode'),
    'description' => t('If order has non-empty code then order in considered for delivery to service point but not to customer address.'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'default' => '[commerce-order:commerce-customer-shipping:field-service-point:external-code]',
    'access' => 'sdek order tokens settings',
  );
  $variables['commerce_sdek_token_BarCode'] = array(
    'title' => t('Order BarCode'),
    'description' => t('Barcode which is used to track the order.'),
    'group' => 'commerce_sdek_order_tokens',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'default' => '[commerce-order:field-parcel-code]',
    'access' => 'sdek order tokens settings',
  );
  
  return $variables;
}
