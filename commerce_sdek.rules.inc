<?php

/**
 * @file
 * Rules actions.
 */

/**
 * Implements hook_rules_event_info().
 */
function commerce_sdek_rules_event_info() {
  $events = array();

  $events['commerce_sdek_order_submitted'] = array(
    'label' => t('Order just been submitted to SDEK system'),
    'group' => t('Commerce SDEK'),
    'variables' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Commerce order'),
      ),
      'code' => array(
        'type' => 'text',
        'label' => t('SDEK order tracking code'),
      ),
      'is_test_mode' => array(
        'type' => 'boolean',
        'label' => t('Is test mode'),
        'default value' => FALSE,
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  return $events;
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_sdek_rules_action_info() {
  $actions = array();

  $actions['commerce_sdek_submit_order'] = array(
    'label' => t('Submit order to SDEK system.'),
    'parameter' => array(
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'save' => FALSE,
      ),
    ),
    'group' => t('Commerce Order'),
    'callbacks' => array(
      'execute' => 'commerce_sdek_submit_order_rules_action',
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  return $actions;
}

/**
 * @param object $commerce_order
 *  Commerce Order Entity.
 * @param array $settings
 * @param RulesState $state
 * @param RulesPlugin $element
 */
function commerce_sdek_submit_order_rules_action($commerce_order, $settings, RulesState $state, RulesPlugin $element) {
  $result = commerce_sdek_submit_order($commerce_order);
  if ($result && isset($result['Code'])) {
    watchdog('commerce_sdek', 'Successfully submit order @order_id', array('@order_id' => $commerce_order->order_id), WATCHDOG_INFO);
  }
  elseif ($result && isset($result['Error'])) {
    watchdog('commerce_sdek', 'Failed to register order @order_id: "@message"', array(
      '@order_id' => $commerce_order->order_id,
      '@message' => $result['Error'],
    ), WATCHDOG_WARNING);
  }
  else {
    watchdog('commerce_sdek', 'Order is not compatible with SDEK processing "<a href ="/admin/commerce/orders/@order_id/edit">@order_id</a>"', array(
      '@order_id' => $commerce_order->order_id,
    ), WATCHDOG_WARNING);
  }
}