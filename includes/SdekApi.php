<?php

/**
 * Sends requests to the server returns error if a timeout occurs.
 */
class SdekApi {

  const xmlAttr = '@attributes';

  /**
   * @var string
   *  Well-formed server url to connect with
   */
  private $serverUrl;

  /**
   * @var int
   *  Timeout for server communications in seconds.
   */
  private $timeout;

  /**
   * @var string
   *  Api login.
   */
  private $login;

  /**
   * @var string
   *  Api password.
   */
  private $password;

  /**
   * @param string $serverUrl
   *  Server url.
   * @param int $timeout
   *  Timeout in seconds.
   * @param string $login
   *  Api login.
   * @param string $password
   *  Api password.
   */
  public function __construct(string $serverUrl, int $timeout, string $login, string $password) {
    $this->serverUrl = $serverUrl;
    $this->timeout = $timeout;
    $this->login = $login;
    $this->password = $password;
  }

  /**
   * @param string $cityCode
   *
   * @return array
   */
  public function getDeliveryPointsList(string $cityCode = ''): array {
    $parameters = array();
    if ($cityCode) {
      $parameters['cityid'] = $cityCode;
    }
    $result = $this->sendGetRequest('pvzlist/v1/xml', $parameters);
    if (!isset($result['Pvz'])) {
      return array();
    }

    // Aligning XML data structure for single node list
    if (isset($result['Pvz'][SdekApi::xmlAttr])) {
      $result['Pvz'] = array($result['Pvz']);
    }

    $points = array_map(function ($xmlData) {
      $point = SdekApi::preprocessSimpleXmlArray($xmlData);
      $point['WorkTimeY'] = array_map(function ($xmlTimeData) {
        return SdekApi::preprocessSimpleXmlArray($xmlTimeData);
      }, $xmlData['WorkTimeY']);
      $point['WeightLimit'] = isset($xmlData['WeightLimit']) ? SdekApi::preprocessSimpleXmlArray($xmlData['WeightLimit']) : array();

      return $point;
    }, $result['Pvz']);

    return $points;
  }

  /**
   * This could be VERY SLOW due to numerous API requests.
   *
   * @return array
   */
  public function getCitiesList(): array {
    $citiesPages = array();
    for ($page = 0; $page < 250; $page++) {
      $pageResult = $this->sendGetRequest('v1/location/cities', array('page' => $page));
      if (!isset($pageResult['Location'])) {
        break;
      }

      $citiesPages[] = array_map(static function (array $city) {
        return array(
          'Name' => $city['cityName'],
          'Code' => $city['cityCode'],
          'countryCode' => $city['countryCode'],
          'regionCode' => $city['regionCode'],
        );
      }, array_map('SdekApi::preprocessSimpleXmlArray', $pageResult['Location']));
    }

    return array_merge(array(), ...$citiesPages);
  }

  /**
   * @param array $parameters
   *
   * @return array
   */
  public function getOrderStatuses(array $parameters = array()): array {
    $data = array();
    $data['Order'][SdekApi::xmlAttr] = $parameters;

    $result = $this->sendXMLRequest('status_report_h.php', 'StatusReport', $data);
    if (isset($result[SdekApi::xmlAttr]['ErrorCode'])) {
      return self::preprocessSimpleXmlArray($result);
    }

    return $result['Order'];
  }

  /**
   * @param array $parameters
   *
   * @return array
   */
  public function getOrderStatus(array $parameters = array()): array {
    $result = $this->sendXMLRequest('status_report_h.php', 'StatusReport', $parameters);
    if (isset($result[SdekApi::xmlAttr]['ErrorCode'])) {
      return self::preprocessSimpleXmlArray($result);
    }

    return isset($result['Order']['Status']) ? self::preprocessSimpleXmlArray($result['Order']['Status']) : self::preprocessSimpleXmlArray($result);
  }

  /**
   * @param array $xmlData
   *
   * @return array
   */
  private static function preprocessSimpleXmlArray(array $xmlData) {
    return $xmlData[SdekApi::xmlAttr] ?? $xmlData;
  }

  /**
   * @param array $parameters
   *
   * @return array
   */
  public function saveOrder(array $parameters): array {
    $result = $this->sendXMLRequest('new_orders.php', 'DeliveryRequest', $parameters);
    if (!isset($result['Order'])) {
      return array();
    }

    return array_map(static function (array $item) {
      return self::preprocessSimpleXmlArray($item);
    }, $result['Order']);
  }
  
  /**
   * @param string $urlSuffix
   * @param string $methodName
   * @param array $parameters
   * 
   * @return array
   *  Resulted data structure representing decoded xml or containing error information.
   */
  private function sendXMLRequest(string $urlSuffix, string $methodName, array $parameters) {
    try {
      $result = simplexml_load_string($this->doPostRequest($urlSuffix, $methodName, $parameters));
      return json_decode(json_encode($result), TRUE);
    }
    catch (Exception $exception) {
      return array('Errors' => array($exception->getMessage()));
    }
  }

  /**
   * @param string $urlSuffix
   * @param array $parameters
   *
   * @return array
   *  Resulted data structure representing decoded xml or containing error information.
   */
  private function sendGetRequest(string $urlSuffix, array $parameters = array()) {
    try {
      $result = simplexml_load_string($this->doGetRequest($urlSuffix, $parameters));
      return json_decode(json_encode($result), TRUE);
    }
    catch (Exception $exception) {
      return array('Errors' => array($exception->getMessage()));
    }
  }
  /**
   * @param int $communication_start_time
   *
   * @return bool
   */
  private function isTimedOut(int $communication_start_time): bool {
    return (time() - $communication_start_time) >= $this->timeout;
  }

  /**
   * Sends data compressed by deflate algorithm to the server.
   *
   * @param string $urlSuffix
   *  Url suffix.
   * @param string $methodName
   * @param array $parameters
   *  Parameters to be send to server.
   *
   * @return string
   *  Xml response.
   * 
   * @throws Exception
   *  If something went wrong when communication with server.
   */
  protected function doPostRequest(string $urlSuffix, string $methodName, array $parameters): string {
    $date = date('Y-m-d');
    $parameters[SdekApi::xmlAttr]['Date'] = $date;
    $parameters[SdekApi::xmlAttr]['Account'] = $this->login;
    $parameters[SdekApi::xmlAttr]['Secure'] = md5("{$date}&{$this->password}");
    $xmlRequest = $this->encodeXML($methodName, $parameters);

    $data = array(
      'xml_request' => $xmlRequest,
    );

    return $this->doSendRequest("{$this->serverUrl}{$urlSuffix}", 'POST', $data);
  }


  /**
   * Sends data compressed by deflate algorithm to the server.
   *
   * @param string $urlSuffix
   *  Url suffix.
   * @param array $parameters
   *  Parameters to be send to server.
   *
   * @return string
   *  Xml response.
   *
   * @throws Exception
   *  If something went wrong when communication with server.
   */
  protected function doGetRequest(string $urlSuffix, array $parameters): string {
    $url = "{$this->serverUrl}{$urlSuffix}";
    if (($query = http_build_query($parameters))) {
      $url .= '?' . $query;
    }
    return $this->doSendRequest($url, 'GET');
  }

  /**
   * @param string $methodName
   * @param array $parameters
   *
   * @return string
   */
  private function encodeXML(string $methodName, array $parameters): string {
    $dom = $this->createDom();
    $method = $this->createXMLNode($dom, $methodName, $parameters);
    $dom->appendChild($method);

    return $dom->saveXML();
  }

  /**
   * @param \DOMDocument $dom
   * @param string $nodeName
   * @param array $nodeInfo
   *
   * @return \DOMElement
   */
  private function createXMLNode(\DOMDocument $dom, string $nodeName, array $nodeInfo): \DOMElement {
    $node = $dom->createElement($nodeName);
    if (isset($nodeInfo[SdekApi::xmlAttr])) {
      foreach ($nodeInfo[SdekApi::xmlAttr] as $name => $value) {
        $node->setAttribute($name, $value);
      }
      unset($nodeInfo[SdekApi::xmlAttr]);
    }
    foreach ($nodeInfo as $childName => $childInfo) {
      if (is_int($childName)) {
        $childName = key($childInfo);
        $childInfo = $childInfo[$childName];
      }
      $node->appendChild($this->createXMLNode($dom, $childName, $childInfo));
    }

    return $node;
  }

  /**
   * @param string $encoding
   *
   * @return \DOMDocument
   */
  private function createDom(string $encoding = 'UTF-8'): \DOMDocument {
    $imp = new \DOMImplementation();
    $dom = $imp->createDocument('', '');
    $dom->encoding = $encoding;

    return $dom;
  }

  /**
   * @param int $communicationStartTime
   * @param string $message
   * 
   * @throws Exception
   */
  private function throwConnectionException(int $communicationStartTime, string $message) {
    $timeoutMsg = $this->isTimedOut($communicationStartTime) ? t('API server response time exceeded (@timeout)', array('@timeout' => $this->timeout)) : '';
    throw new Exception($message . ' ' . $timeoutMsg);
  }

  /**
   * @param string $url
   * @param string $httpMethod
   * @param array $data
   *
   * @return string
   * @throws Exception
   */
  private function doSendRequest(string $url, string $httpMethod, array $data = array()): string {
    $requestOptions = array(
      'method' => $httpMethod,
      'timeout' => $this->timeout,
      'headers' => array(
        'Content-type' => 'application/x-www-form-urlencoded;charset="utf-8"',
      ),
    );
    if ($httpMethod === 'POST') {
      $requestOptions['data'] = http_build_query($data);
    }


    $communicationStartTime = time();
    $response = drupal_http_request($url, $requestOptions);

    if (isset($response->error)) {
      $this->throwConnectionException($communicationStartTime, t("Problem occurred while connecting to API's server."));
    }

    if (!isset($response->data) || empty($response->data)) {
      $this->throwConnectionException($communicationStartTime, t("Problem occurred while reading data from API's server."));
    }

    return $response->data;
  }

}
